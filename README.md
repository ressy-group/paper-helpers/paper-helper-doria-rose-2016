# Antibody GenBank entries from Doria-Rose 2016

New Member of the V1V2-Directed CAP256-VRC26 Lineage That Shows Increased Breadth and Exceptional Potency.
Doria-Rose et al.
Virology 90:76-91.
<https://doi.org/10.1128/JVI.01791-15>

See also [paper-helper-doria-rose-2014](https://gitlab.com/ressy-group/paper-helpers/paper-helper-doria-rose-2014).

 * Subject: CAP256
 * Antibody lineage: VRC26
 * Antibody specificity: V2 apex

GenBank:

 * CAP256-VRC26 mAbs 13 to 33: GenBank KT371076 to KT371117
 * Lineage members from NGS: GenBank KT371118 to KT371320
 * NGS from CAP256 at week 34: SRA SRR2126754 and SRR2126755 (is one heavy
   chain and one light?)
 * CAP256-VRC26.25 structures: PDB 5DT1
